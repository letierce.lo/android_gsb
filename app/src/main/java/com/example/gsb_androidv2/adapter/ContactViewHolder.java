package com.example.gsb_androidv2.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.gsb_androidv2.R;

public class ContactViewHolder extends RecyclerView.ViewHolder {

    public TextView name,address,ph_no;
    public ImageView deleteContact;
    public  ImageView editContact;

    public ContactViewHolder(View itemView) {
        super(itemView);
        name = (TextView)itemView.findViewById(R.id.contact_name);
        address = (TextView)itemView.findViewById(R.id.contact_address);
        ph_no = (TextView)itemView.findViewById(R.id.contact_phone);
        deleteContact = (ImageView)itemView.findViewById(R.id.delete_contact);
        editContact = (ImageView)itemView.findViewById(R.id.edit_contact);
    }
}
